/*
 Navicat Premium Data Transfer

 Source Server         : V1
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : v1.sukabumiwebsolution.com:3306
 Source Schema         : pengaduan

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 24/08/2020 02:26:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hasil_laporan
-- ----------------------------
DROP TABLE IF EXISTS `hasil_laporan`;
CREATE TABLE `hasil_laporan`  (
  `notlhp` int(0) NOT NULL AUTO_INCREMENT,
  `nolaporan` int(0) NOT NULL,
  `hasil_laporan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `status_laporan` enum('BARU','PROSES','SELESAI') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`notlhp`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hasil_laporan
-- ----------------------------
INSERT INTO `hasil_laporan` VALUES (1, 28, 'Testing Bro', '2020-08-23 16:27:32', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (2, 28, 'Test Lagi', '2020-08-23 16:27:32', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (3, 28, 'Testing we', '2020-08-23 16:27:32', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (4, 28, 'done', '2020-08-23 16:38:39', 'SELESAI');
INSERT INTO `hasil_laporan` VALUES (5, 30, 'sudah di tanggapi', '2020-08-23 19:09:07', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (6, 31, 'Sudah di tanggapi', '2020-08-24 00:36:00', 'SELESAI');
INSERT INTO `hasil_laporan` VALUES (7, 32, 'olah tkp', '2020-08-24 00:50:54', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (8, 34, 'test', '2020-08-23 18:06:05', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (9, 34, 'Done bro', '2020-08-23 18:07:28', 'SELESAI');
INSERT INTO `hasil_laporan` VALUES (10, 33, 'Testing OM', '2020-08-23 18:08:55', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (11, 34, 'sedang di evaluasi lebih lanjut', '2020-08-23 20:49:52', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (12, 35, 'sedang di evaluasi', '2020-08-23 21:22:08', 'PROSES');
INSERT INTO `hasil_laporan` VALUES (13, 35, 'sudah beres', '2020-08-23 21:25:00', 'SELESAI');

-- ----------------------------
-- Table structure for laporan
-- ----------------------------
DROP TABLE IF EXISTS `laporan`;
CREATE TABLE `laporan`  (
  `idnolaporan` int(0) NOT NULL AUTO_INCREMENT,
  `id_users` int(0) NOT NULL,
  `isi_laporan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lokasi_kejadian` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_kejadian` date NOT NULL,
  `jam_kejadian` time(6) NOT NULL,
  `tanggal` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `nominal_kerugian` int(0) NOT NULL,
  `status_laporan` enum('BARU','PROSES','SELESAI') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idnolaporan`) USING BTREE,
  INDEX `user`(`id_users`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of laporan
-- ----------------------------
INSERT INTO `laporan` VALUES (24, 5, 'test', 'test', '2020-08-21', '20:19:19.000000', '2020-08-22 21:39:47', 0, 'BARU');
INSERT INTO `laporan` VALUES (25, 6, 'test', 'test', '2020-08-13', '20:19:52.000000', '2020-08-22 21:39:54', 0, 'SELESAI');
INSERT INTO `laporan` VALUES (26, 5, 'Beras bantuan jelek', 'RT 2/3', '2020-08-20', '12:00:00.000000', '2020-08-22 21:39:51', 150000, 'PROSES');
INSERT INTO `laporan` VALUES (27, 5, 'a', 'RT 2/3', '2000-10-10', '10:10:00.000000', '2020-08-22 15:12:28', 0, 'BARU');
INSERT INTO `laporan` VALUES (28, 8, 'longsor', 'jl.karamat', '2020-02-08', '15:00:00.000000', '2020-08-23 23:38:39', 0, 'SELESAI');
INSERT INTO `laporan` VALUES (29, 8, 'bantuan langsung tunai', 'jl.karamat', '2020-02-23', '03:30:00.000000', '2020-08-24 00:14:00', 0, 'BARU');
INSERT INTO `laporan` VALUES (30, 8, 'blt', 'jl. karamat. 03/04', '2020-08-24', '16:42:00.000000', '2020-08-24 00:09:07', 0, 'PROSES');
INSERT INTO `laporan` VALUES (31, 15, 'Laporan ptls', 'Jl. Karamat rt. 02/03', '2020-08-24', '00:35:00.000000', '2020-08-24 00:36:00', 0, 'SELESAI');
INSERT INTO `laporan` VALUES (32, 16, 'Tanah longosr', 'Jl. Kopeng RT . 02/05', '2020-08-24', '00:38:00.000000', '2020-08-24 00:50:54', 0, 'PROSES');
INSERT INTO `laporan` VALUES (33, 15, 'a', 'a', '2020-10-10', '10:10:00.000000', '2020-08-24 01:08:55', 50, 'PROSES');
INSERT INTO `laporan` VALUES (34, 15, 'a', 'p', '2020-10-10', '10:10:00.000000', '2020-08-20 02:00:22', 0, 'BARU');
INSERT INTO `laporan` VALUES (35, 18, 'Longsor', 'jl. kopeng rt. 02/05', '2020-08-22', '00:00:00.000000', '2020-08-24 02:25:00', 12000000, 'SELESAI');

-- ----------------------------
-- Table structure for tbl_hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `tbl_hak_akses`;
CREATE TABLE `tbl_hak_akses`  (
  `id_users` int(0) NOT NULL AUTO_INCREMENT,
  `id_user_level` int(0) NOT NULL,
  `id_menu` int(0) NOT NULL,
  PRIMARY KEY (`id_users`) USING BTREE,
  INDEX `id_user_level`(`id_user_level`) USING BTREE,
  INDEX `id_menu`(`id_menu`) USING BTREE,
  INDEX `id_users`(`id_users`) USING BTREE,
  CONSTRAINT `tbl_hak_akses_ibfk_1` FOREIGN KEY (`id_user_level`) REFERENCES `tbl_user_level` (`id_user_level`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbl_hak_akses_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `tbl_menu` (`id_menu`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_hak_akses
-- ----------------------------
INSERT INTO `tbl_hak_akses` VALUES (15, 1, 1);
INSERT INTO `tbl_hak_akses` VALUES (19, 1, 3);
INSERT INTO `tbl_hak_akses` VALUES (24, 1, 9);
INSERT INTO `tbl_hak_akses` VALUES (30, 1, 2);
INSERT INTO `tbl_hak_akses` VALUES (31, 1, 10);
INSERT INTO `tbl_hak_akses` VALUES (32, 1, 11);
INSERT INTO `tbl_hak_akses` VALUES (34, 1, 12);
INSERT INTO `tbl_hak_akses` VALUES (36, 2, 10);
INSERT INTO `tbl_hak_akses` VALUES (39, 2, 11);

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu`  (
  `id_menu` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_main_menu` int(0) NOT NULL,
  `is_aktif` enum('y','n') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'y=yes,n=no',
  PRIMARY KEY (`id_menu`) USING BTREE,
  INDEX `id_menu`(`id_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO `tbl_menu` VALUES (1, 'KELOLA MENU', 'kelolamenu', 'fa fa-server', 0, 'y');
INSERT INTO `tbl_menu` VALUES (2, 'KELOLA PENGGUNA', 'user', 'fa fa-user-o', 0, 'y');
INSERT INTO `tbl_menu` VALUES (3, 'level PENGGUNA', 'userlevel', 'fa fa-users', 0, 'y');
INSERT INTO `tbl_menu` VALUES (9, 'Contoh Form', 'welcome/form', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (10, 'Laporan', 'Laporan', 'fa', 0, 'y');
INSERT INTO `tbl_menu` VALUES (11, 'List Laporan', 'Laporan', 'fa', 10, 'y');
INSERT INTO `tbl_menu` VALUES (12, 'Hasil Laporan', 'Hasil_laporan', 'fa', 10, 'y');

-- ----------------------------
-- Table structure for tbl_setting
-- ----------------------------
DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting`  (
  `id_setting` int(0) NOT NULL AUTO_INCREMENT,
  `nama_setting` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `value` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_setting`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_setting
-- ----------------------------
INSERT INTO `tbl_setting` VALUES (1, 'Tampil Menu', 'ya');

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user`  (
  `id_users` int(0) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `images` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_user_level` int(0) NOT NULL,
  `is_aktif` enum('y','n') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nokk` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nik` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_hp` varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_users`) USING BTREE,
  INDEX `id_user_level`(`id_user_level`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES (1, 'Galih Setyo Alam', 'galihsa666@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 1, 'y', NULL, NULL, NULL);
INSERT INTO `tbl_user` VALUES (4, 'Petugas', 'karamatkahiji123@gmail.com', '$2y$04$3l10xXOkQJk5ocCmGk.lZ.4JJutEopyJ0yyN7Bk6ZGvvMjr2dhcNG', 'AIbEiAIAAABDCKXuiqCLy6WteCILdmNhcmRfcGhvdG8qKDBkYTU3NDYzMjJmZDRlOTg4NzQ1NzcxYmEyMTMwNzg5NTMzNzU5MjIwAc2aSJgyc6O4PJgVnLXnnC-c2Edk.jpg', 1, 'y', NULL, NULL, NULL);
INSERT INTO `tbl_user` VALUES (7, 'Muhammad Rizal Fadhilah', 'rizal90@gmail.com', '$2y$04$mzOC7xqq8uL5ZBb4BWdVjOUQ3jkMsNN/4soGNYQxTqy6LFbg0t3A.', '', 1, 'y', NULL, NULL, NULL);
INSERT INTO `tbl_user` VALUES (14, 'yuniar', 'yuniarivine@gmail.com', '$2y$04$9g7iaMo8wgfv3Xu1AofEJ.4QwKPwRrKihTSeW49.EvpmltdqfmFMi', '', 1, 'y', NULL, NULL, NULL);
INSERT INTO `tbl_user` VALUES (18, 'rismawan junandia', 'wan@gmail.com', '$2y$10$D/h2wTgHwlp66T5i1h86MutGu.6kRm.BjVjmTX7d1Y7826H/p4QGG', NULL, 2, 'y', '3272015001980022', '3272013010505023', '083871370378');

-- ----------------------------
-- Table structure for tbl_user_level
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_level`;
CREATE TABLE `tbl_user_level`  (
  `id_user_level` int(0) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_user_level`) USING BTREE,
  INDEX `id_user_level`(`id_user_level`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_user_level
-- ----------------------------
INSERT INTO `tbl_user_level` VALUES (1, 'PETUGAS');
INSERT INTO `tbl_user_level` VALUES (2, 'PELAPOR');

SET FOREIGN_KEY_CHECKS = 1;
