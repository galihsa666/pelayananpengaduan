<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hasil_laporan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Hasil_laporan_model');
        $this->load->model('Laporan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/hasil_laporan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/hasil_laporan/index/';
            $config['first_url'] = base_url() . 'index.php/hasil_laporan/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->Hasil_laporan_model->total_rows($q);
        $hasil_laporan = $this->Hasil_laporan_model->get_limit_data($config['per_page'], $start, $q);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'hasil_laporan_data' => $hasil_laporan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','hasil_laporan/hasil_laporan_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Hasil_laporan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'notlhp' => $row->notlhp,
		'tgl_tlhp' => $row->tgl_tlhp,
		'nolaporan' => $row->nolaporan,
		'hasil_laporan' => $row->hasil_laporan,
	    );
            $this->template->load('template','hasil_laporan/hasil_laporan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hasil_laporan'));
        }
    }

    public function create($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);
        if($row){
            $data_lapor = array(
                'full_name' => $row->full_name,
                'isi_laporan' => $row->isi_laporan,
                'lokasi_kejadian' => $row->lokasi_kejadian,
                'tgl_kejadian' => $row->tgl_kejadian,
                'jam_kejadian' => $row->jam_kejadian,
                'tanggal' => $row->tanggal,
                'nominal_kerugian' => $row->nominal_kerugian,
                'status_laporan' => $row->status_laporan,
                'no_hp' => $row->no_hp
            );
        }else{
            $data_lapor = 'No data found';
        }
        $data = array(
            'button' => 'Create',
            'action' => site_url('hasil_laporan/create_action'),
            'laporan_masyarakat' => $data_lapor,
            'hasil' => $this->Hasil_laporan_model->get_by_id($id),
	    'notlhp' => set_value('notlhp'),
	    'nolaporan' => $id,
        'hasil_laporan' => set_value('hasil_laporan'),
	);
        $this->template->load('template','hasil_laporan/hasil_laporan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create($this->input->post('nolaporan'));
        } else {
            $data = array(
		'nolaporan' => $this->input->post('nolaporan',TRUE),
        'hasil_laporan' => $this->input->post('hasil_laporan',TRUE),
        'created_at' => date('Y-m-d H:i:s'),
        'status_laporan' => $this->input->post('status_laporan', TRUE)
        );
            $status = [
                'status_laporan' => $this->input->post('status_laporan', TRUE)
            ];
            $this->Laporan_model->update($this->input->post('nolaporan'), $status);
            $this->Hasil_laporan_model->insert($data);
            $nohp = $this->input->post('no_hp',TRUE);
            $msg = "Laporan pengaduan dengan nomor laporan #".$this->input->post('nolaporan',TRUE)." Telah mendapatkan response : \n".$this->input->post('hasil_laporan',TRUE)."\nStatus Laporan : ".$this->input->post('status_laporan', TRUE)."\n\nKelurahan Karamat";
            $this->Laporan_model->kirimsms($nohp, $msg);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(base_url('index.php/Hasil_laporan/create/'.$this->input->post('nolaporan', TRUE)));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Hasil_laporan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('hasil_laporan/update_action'),
		'notlhp' => set_value('notlhp', $row->notlhp),
		'tgl_tlhp' => set_value('tgl_tlhp', $row->tgl_tlhp),
		'nolaporan' => set_value('nolaporan', $row->nolaporan),
		'hasil_laporan' => set_value('hasil_laporan', $row->hasil_laporan),
	    );
            $this->template->load('template','hasil_laporan/hasil_laporan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hasil_laporan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('notlhp', TRUE));
        } else {
            $data = array(
		'tgl_tlhp' => $this->input->post('tgl_tlhp',TRUE),
		'nolaporan' => $this->input->post('nolaporan',TRUE),
		'hasil_laporan' => $this->input->post('hasil_laporan',TRUE),
	    );

            $this->Hasil_laporan_model->update($this->input->post('notlhp', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('hasil_laporan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Hasil_laporan_model->get_by_id($id);

        if ($row) {
            $this->Hasil_laporan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('hasil_laporan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hasil_laporan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nolaporan', 'nolaporan', 'trim|required');
	$this->form_validation->set_rules('hasil_laporan', 'hasil laporan', 'trim|required');

	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function cetak($id)
    {
        $row = $this->Laporan_model->get_by_id($id);
        if($row){
            $data_lapor = array(
                'full_name' => $row->full_name,
                'isi_laporan' => $row->isi_laporan,
                'lokasi_kejadian' => $row->lokasi_kejadian,
                'tgl_kejadian' => $row->tgl_kejadian,
                'jam_kejadian' => $row->jam_kejadian,
                'tanggal' => $row->tanggal,
                'nominal_kerugian' => $row->nominal_kerugian,
                'status_laporan' => $row->status_laporan
            );
        }else{
            $data_lapor = 'No data found';
        }
        $data = array(
            'button' => 'Create',
            'action' => site_url('hasil_laporan/create_action'),
            'laporan_masyarakat' => $data_lapor,
            'hasil' => $this->Hasil_laporan_model->get_by_id($id),
            'notlhp' => set_value('notlhp'),
            'nolaporan' => $id,
            'hasil_laporan' => set_value('hasil_laporan'),
        );

        $this->load->view('hasil_laporan/cetak', $data);
    }

}

/* End of file Hasil_laporan.php */
/* Location: ./application/controllers/Hasil_laporan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-08-21 06:22:58 */
/* http://harviacode.com */