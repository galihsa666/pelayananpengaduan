<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Laporan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/laporan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/laporan/index/';
            $config['first_url'] = base_url() . 'index.php/laporan/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->Laporan_model->total_rows($q);
        $id_users = ($this->session->userdata('id_user_level') == 2) ? $this->session->userdata('id_users') : null;
        $laporan = $this->Laporan_model->get_limit_data($config['per_page'], $start, $q, $id_users);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'laporan_data' => $laporan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','laporan/laporan_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'idnolaporan' => $row->idnolaporan,
		'noktp' => $row->noktp,
		'isi_laporan' => $row->isi_laporan,
		'lokasi_kejadian' => $row->lokasi_kejadian,
		'tgl_kejadian' => $row->tgl_kejadian,
		'jam_kejadian' => $row->jam_kejadian,
		'tanggal' => $row->tanggal,
		'nominal_kerugian' => $row->nominal_kerugian,
	    );
            $this->template->load('template','laporan/laporan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('laporan/create_action'),
	    'idnolaporan' => set_value('idnolaporan'),
	    'noktp' => set_value('noktp'),
	    'isi_laporan' => set_value('isi_laporan'),
	    'lokasi_kejadian' => set_value('lokasi_kejadian'),
	    'tgl_kejadian' => set_value('tgl_kejadian'),
	    'jam_kejadian' => set_value('jam_kejadian'),
	    'tanggal' => date('Y-m-d H:i:s'),
	    'nominal_kerugian' => set_value('nominal_kerugian'),
        'id_users' => $this->session->userdata('id_users')
	);
        $this->template->load('template','laporan/laporan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_users' => $this->input->post('id_users',TRUE),
		'isi_laporan' => $this->input->post('isi_laporan',TRUE),
		'lokasi_kejadian' => $this->input->post('lokasi_kejadian',TRUE),
		'tgl_kejadian' => $this->input->post('tgl_kejadian',TRUE),
		'jam_kejadian' => $this->input->post('jam_kejadian',TRUE),
		'tanggal' => $this->input->post('tanggal',TRUE),
		'nominal_kerugian' => $this->input->post('nominal_kerugian',TRUE),
        'status_laporan' => 'BARU'
        );
            $nohp = $this->Laporan_model->nohp($this->input->post('id_users',TRUE));
            $msg = "Laporan anda berhasil kami terima.\n\nKelurahan Karamat";
            $this->Laporan_model->kirimsms($nohp, $msg);
            $this->Laporan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('laporan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('laporan/update_action'),
		'idnolaporan' => set_value('idnolaporan', $row->idnolaporan),
		'noktp' => set_value('noktp', $row->noktp),
		'isi_laporan' => set_value('isi_laporan', $row->isi_laporan),
		'lokasi_kejadian' => set_value('lokasi_kejadian', $row->lokasi_kejadian),
		'tgl_kejadian' => set_value('tgl_kejadian', $row->tgl_kejadian),
		'jam_kejadian' => set_value('jam_kejadian', $row->jam_kejadian),
		'tanggal' => set_value('tanggal', $row->tanggal),
		'nominal_kerugian' => set_value('nominal_kerugian', $row->nominal_kerugian),
	    );
            $this->template->load('template','laporan/laporan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('idnolaporan', TRUE));
        } else {
            $data = array(
		'noktp' => $this->input->post('noktp',TRUE),
		'isi_laporan' => $this->input->post('isi_laporan',TRUE),
		'lokasi_kejadian' => $this->input->post('lokasi_kejadian',TRUE),
		'tgl_kejadian' => $this->input->post('tgl_kejadian',TRUE),
		'jam_kejadian' => $this->input->post('jam_kejadian',TRUE),
		'tanggal' => $this->input->post('tanggal',TRUE),
		'nominal_kerugian' => $this->input->post('nominal_kerugian',TRUE),
	    );

            $this->Laporan_model->update($this->input->post('idnolaporan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('laporan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Laporan_model->get_by_id($id);

        if ($row) {
            $this->Laporan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('laporan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('isi_laporan', 'isi laporan', 'trim|required');
	$this->form_validation->set_rules('lokasi_kejadian', 'lokasi kejadian', 'trim|required');
	$this->form_validation->set_rules('tgl_kejadian', 'tgl kejadian', 'trim|required');
	$this->form_validation->set_rules('jam_kejadian', 'jam kejadian', 'trim|required');
	$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
	$this->form_validation->set_rules('nominal_kerugian', 'nominal kerugian', 'trim|required');

	$this->form_validation->set_rules('idnolaporan', 'idnolaporan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function kirim_pesan($id){
        $row = $this->Laporan_model->get_by_id($id);
        $nohp = $row->no_hp;
        $msg = "Laporan pengaduan dengan id pengaduan #".$id." sudah Kadalwarsa, silahkan membuat ulang laporan.\n\nKelurahan Karamat";
        $this->Laporan_model->kirimsms($nohp, $msg);
        $this->session->set_flashdata('message',alert('alert-info', 'Berhasil kirim pengingat', 'Pengingat berhasil terkirim.'));
            redirect('Laporan');
    }

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-08-21 06:23:01 */
/* http://harviacode.com */