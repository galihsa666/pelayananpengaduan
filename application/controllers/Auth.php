<?php
Class Auth extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
    }
    
    function index(){
        $this->load->view('auth/login');
    }

    function daftar(){
        $this->load->view('auth/daftar');
    }
    
    function cheklogin(){
        $email      = $this->input->post('email');
        //$password   = $this->input->post('password');
        $password = $this->input->post('password',TRUE);
        //$hashPass = password_hash($password,PASSWORD_DEFAULT);
        //$test     = password_verify($password, $hashPass);
        // query chek users
        $this->db->where('email',$email);
        //$this->db->where('password',  $test);
        $users       = $this->db->get('tbl_user');
        if($users->num_rows()>0){
            $user = $users->row_array();
            if(password_verify($password,$user['password'])){
                // retrive user data to session
                $this->session->set_userdata($user);
                redirect('welcome');
            }else{
                $this->session->set_flashdata('status_login',alert('alert-danger', 'Masuk Gagal', 'Email atau Password tidak ditemukan.'));
                redirect('auth');
            }
        }else{
            $this->session->set_flashdata('status_login',alert('alert-danger', 'Masuk Gagal', 'Email atau Password tidak ditemukan.'));
            redirect('auth');
        }
    }
    
    function logout(){
        $this->session->sess_destroy();
        $this->session->set_flashdata('status_login',alert('alert-success', 'Berhasil Keluar', 'Anda sudah berhasil keluar dari aplikasi'));
        redirect('auth');
    }

    function daftar_aksi(){
        $data = [
            'full_name'     => $this->input->post('full_name', TRUE),
            'email'         => $this->input->post('email', TRUE),
            'password'      => password_hash($this->input->post('password', TRUE),PASSWORD_DEFAULT),
            'id_user_level' => 2,
            'is_aktif'      => 'y',
            'nokk'          => $this->input->post('nokk',TRUE),
            'nik'           => $this->input->post('nik',TRUE),
            'no_hp'         => $this->input->post('no_hp',TRUE),
        ];
            $aksi = $this->Auth_model->daftar($data);
                $this->session->set_flashdata('status_login',alert('alert-success', 'Pendaftaran berhasil', 'Silahkan masuk untuk membuat laporan'));
            redirect('auth');
    }
}
