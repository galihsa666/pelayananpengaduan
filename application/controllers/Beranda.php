<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model');
    }

    public function index()
    {

        $row = $this->Laporan_model->total();
        $data = [
            'proses' => $row->proses,
            'selesai' => $row->selesai,
            'terabaikan' => $row->terabaikan,
            'total' => $row->total
        ];
        $this->load->view('beranda', $data);
    } 
}