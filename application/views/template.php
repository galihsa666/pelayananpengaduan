<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>KARAMAT NGAHIJI</title>

    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/adminlte/') ?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url('/') ?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url('/') ?>assets/font-awesome/4.2.0/css/font-awesome.min.css" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="<?php echo base_url('/') ?>assets/fonts/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo base_url('/') ?>assets/css/ace.min.css" class="ace-main-stylesheet"
        id="main-ace-style" />

    <style>
        .box-header{
            height: 40px;
        }
    </style>
    <script src="assets/js/ace-extra.min.js"></script>

</head>

<body class="no-skin">
    <div id="navbar" class="navbar navbar-default">
        <script type="text/javascript">
            try {
                ace.settings.check('navbar', 'fixed')
            } catch (e) {}
        </script>

        <div class="navbar-container" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>

            <div class="navbar-header pull-left">
                <a href="?view=beranda" class="navbar-brand">
                    <small>
                        <img src="<?php echo base_url('/') ?>assets/img/lambang.png" width="50px" />
                        KARAMAT KAHIJI
                    </small>
                </a>
            </div>

            <div class="navbar-buttons navbar-header pull-right" role="navigation">
                <ul class="nav ace-nav">



                    <li class="light-black">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <img class="nav-user-photo" src="assets/img/kelurahan.jpg" alt="" />
                            <span class="user-info">
                                <small>Selamat Datang,</small>
                                <?php echo $this->session->userdata('full_name'); ?>
                            </span>

                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>

                        <ul
                            class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">




                            <li class="divider"></li>

                            <li>
                                <a href="<?php echo base_url('index.php/Auth/logout') ?>">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div><!-- /.navbar-container -->
    </div>

    <div class="main-container" id="main-container">
        <script type="text/javascript">
            try {
                ace.settings.check('main-container', 'fixed')
            } catch (e) {}
        </script>

        <div id="sidebar" class="sidebar                  responsive">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {}
            </script>


            <ul class="nav nav-list">
                <li class="active">
                    <a href="<?php echo base_url('index.php/Welcome') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text"> Beranda </span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <?php
                if ($this->session->userdata('id_user_level') == 2) {
                    ?>
                <li class="">
                    <a href="<?php echo base_url('index.php/Tbl_user/update/') ?>">
                        <i class="menu-icon fa fa-list-alt"></i>
                        <span class="menu-text"> Identitas Pelapor </span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <?php
                }else{
                    ?>
                <li class="">
                    <a href="<?php echo base_url('index.php/Tbl_user/') ?>">
                        <i class="menu-icon fa fa-list-alt"></i>
                        <span class="menu-text"> Identitas Pelapor </span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <?php
                }
                ?>

                <li class="">
                    <a href="<?php echo base_url('index.php/Laporan'); ?>">
                        <i class="menu-icon fa fa-list-alt"></i>
                        <span class="menu-text"> Laporan </span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <?php
                if($this->session->userdata('id_user_level') == 1){
                    ?>
                <li class="">
                    <a href="<?php echo base_url('index.php/User/') ?>">
                        <i class="menu-icon fa fa-list-alt"></i>
                        <span class="menu-text"> Petugas </span>
                    </a>
                    <b class="arrow"></b>
                </li>
                <?php
                }
                ?>
            </ul><!-- /.nav-list -->

            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left"
                    data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>

            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {}
            </script>
        </div>
        <p><?php echo $contents ?></p>
        

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </a>
    </div>

    <script src="assets/js/jquery.2.1.1.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='<?php echo base_url('/') ?>assets/js/jquery.min.js'>" + "<" +
            "/script>");
    </script>


    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write(
            "<script src='<?php echo base_url('/') ?>assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="<?php echo base_url('/') ?>assets/js/bootstrap.min.js"></script>


    <script src="<?php echo base_url('/') ?>assets/js/jquery-ui.custom.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.ui.touch-punch.min.js"></script>

    <script src="<?php echo base_url('/') ?>assets/js/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.flot.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.flot.pie.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.flot.resize.min.js"></script>

    <script src="<?php echo base_url('/') ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/dataTables.tableTools.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/dataTables.colVis.min.js"></script>
    <!-- ace scripts -->
    <script src="<?php echo base_url('/') ?>assets/js/jquery.knob.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.autosize.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/bootstrap-tag.min.js"></script>

    <script src="<?php echo base_url('/') ?>assets/js/ace-elements.min.js"></script>
    <script src="<?php echo base_url('/') ?>assets/js/ace.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#dynamic-table').DataTable();
        });
    </script>
    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function ($) {

            /////////
            $('#modal-form input[type=file]').ace_file_input({
                style: 'well',
                btn_choose: 'Pilih Foto',
                btn_change: null,
                no_icon: 'ace-icon fa fa-camera',
                droppable: true,
                thumbnail: 'large'
            })
            $('#modal-edit input[type=file]').ace_file_input({
                style: 'well',
                btn_choose: 'Pilih Foto',
                btn_change: null,
                no_icon: 'ace-icon fa fa-camera',
                droppable: true,
                thumbnail: 'large'
            })

            //chosen plugin inside a modal will have a zero width because the select element is originally hidden
            //and its width cannot be determined.
            //so we set the width after modal is show
            $('#modal-form').on('shown.bs.modal', function () {
                if (!ace.vars['touch']) {
                    $(this).find('.chosen-container').each(function () {
                        $(this).find('a:first-child').css('width', '210px');
                        $(this).find('.chosen-drop').css('width', '210px');
                        $(this).find('.chosen-search input').css('width', '200px');
                    });
                }
            })
            $('#modal-edit').on('shown.bs.modal', function () {
                if (!ace.vars['touch']) {
                    $(this).find('.chosen-container').each(function () {
                        $(this).find('a:first-child').css('width', '210px');
                        $(this).find('.chosen-drop').css('width', '210px');
                        $(this).find('.chosen-search input').css('width', '200px');
                    });
                }
            })

        })
    </script>
</body>

</html>