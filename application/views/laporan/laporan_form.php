<div class="content-wrapper">

	<section class="content">
		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">INPUT DATA LAPORAN</h3>
			</div>
			<div class="box-body">
				<form action="<?php echo $action; ?>" method="post">

					<table class='table table-bordered'>
						<?php
	    	if ($this->session->userdata('id_user_level') == 1) {
	    ?>
						<tr>
							<td width="200">Pelapor</td>
							<td><?php echo select2_dinamis('id_users','tbl_user','full_name','Pilih Pelapor','id_users') ?>
							</td>
						</tr>
						<?php
	    }
	    ?>
						<tr>
							<td width='200'>Lokasi Kejadian <?php echo form_error('lokasi_kejadian') ?>
							</td>
							<td>
								<input type="text" class="form-control" name="lokasi_kejadian" id="lokasi_kejadian"
									placeholder="Lokasi Kejadian" value="<?php echo $lokasi_kejadian; ?>" />
							</td>
						</tr>
						<tr>
							<td width='200'>Tgl Kejadian <?php echo form_error('tgl_kejadian') ?>
							</td>
							<td>
								<input type="date" class="form-control" name="tgl_kejadian" id="tgl_kejadian"
									placeholder="Tgl Kejadian" value="<?php echo $tgl_kejadian; ?>" />
							</td>
						</tr>
						<tr>
							<td width='200'>Jam Kejadian <?php echo form_error('jam_kejadian') ?>
							</td>
							<td>
								<input type="time" class="form-control" name="jam_kejadian" id="jam_kejadian"
									placeholder="Jam Kejadian" value="<?php echo $jam_kejadian; ?>" />
							</td>
						</tr>
						<tr>
							<td width='200'>Nominal Kerugian <?php echo form_error('nominal_kerugian') ?>
							</td>
							<td>
								<input type="text" class="form-control" name="nominal_kerugian" id="nominal_kerugian"
									placeholder="Nominal Kerugian" value="<?php echo $nominal_kerugian; ?>" />
							</td>
						</tr>
						<tr>
							<td width='200'>Isi Laporan <?php echo form_error('isi_laporan') ?>
							</td>
							<td>
								<textarea class="form-control" rows="3" name="isi_laporan" id="isi_laporan"
									placeholder="Isi Laporan"><?php echo $isi_laporan; ?></textarea>
							</td>
						</tr>
						<tr>
							<td>

							</td>
							<td>
								<?php
	    			if ($this->session->userdata('id_user_level') == 2) {
	    		?>
								<input type="hidden" class="form-control" name="id_users" id="id_users"
									placeholder="id_users" value="<?php echo $id_users; ?>" />

								<?php
	    			}
	    		?>
								<input type="hidden" class="form-control" name="tanggal" id="tanggal"
									placeholder="Tanggal" value="<?php echo $tanggal; ?>" />
								<input type="hidden" name="idnolaporan" value="<?php echo $idnolaporan; ?>" />
								<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
									<?php echo $button ?></button>
								<a href="<?php echo site_url('laporan') ?>" class="btn btn-info"><i
										class="fa fa-sign-out"></i> Kembali</a>
							</td>
						</tr>

					</table>
				</form>
			</div>
		</div>
	</section>
</div>