<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">IDENTITAS PELAPOR</h3>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-4 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
                <div class="col-md-1 text-right">
                </div>
                <div class="col-md-3 text-right">

                </div>
            </div>
            <form action="<?php echo $action; ?>" method="post">

                <table class='table table-bordered'>
                    <tr>
                        <td width='200'>Full Name <?php echo form_error('full_name') ?></td>
                        <td><input type="text" class="form-control" name="full_name" id="full_name"
                                placeholder="Full Name" value="<?php echo $full_name; ?>" /></td>
                    </tr>
                    <tr>
                        <td width='200'>Email <?php echo form_error('email') ?></td>
                        <td><input type="text" class="form-control" name="email" id="email" placeholder="Email"
                                value="<?php echo $email; ?>" /></td>
                    </tr>
                    <tr>
                        <td width='200'>Password <?php echo form_error('password') ?></td>
                        <td><input type="text" class="form-control" name="password" id="password" placeholder="Password"
                                value="" /></td>
                    </tr>

                    <tr>
                        <td width='200'>Nokk <?php echo form_error('nokk') ?></td>
                        <td><input type="text" class="form-control" name="nokk" id="nokk" placeholder="Nokk"
                                value="<?php echo $nokk; ?>" /></td>
                    </tr>
                    <tr>
                        <td width='200'>Nik <?php echo form_error('nik') ?></td>
                        <td><input type="text" class="form-control" name="nik" id="nik" placeholder="Nik"
                                value="<?php echo $nik; ?>" /></td>
                    </tr>
                    <tr>
                        <td width='200'>No Hp <?php echo form_error('no_hp') ?></td>
                        <td><input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="No Hp"
                                value="<?php echo $no_hp; ?>" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="id_users" value="<?php echo $id_users; ?>" />
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?php echo $button ?></button>
                            <a href="<?php echo site_url('tbl_user') ?>" class="btn btn-info"><i
                                    class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
</div>