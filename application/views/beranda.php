<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="icon" type="img/png" href="http://example.com/admin/asset/img/lambang.png">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>KELURAHAN KARAMAT</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- FONT AWESOME ICONS  -->
    <link href="<?php echo base_url('assets/beranda/') ?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="<?php echo base_url('assets/beranda/') ?>assets/css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url('assets/beranda/') ?>css/pogo-slider.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/adminlte/') ?>dist/css/AdminLTE.min.css">
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <strong>Email: </strong>karamatkahiji123@gmail.com
                    &nbsp;&nbsp;
                    <strong>Support: </strong>+6283871370378
                </div>
                <div class="col-md-6">
                    <a href="<?php echo base_url('index.php/Auth') ?>"
                        style="color:white; text-decoration: none;">Login</a>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <style type="text/css">
                    navbar-inverse {
                        background-color: grey;
                        font-size: 18px;
                    }
                </style>



                <center> <img src="<?php echo base_url('assets/') ?>img/lambang.png" width="180px" /></center>


            </div>
            <div class="navbar-header">



                <h1>
                    <style type="text/css">
                        body {
                            font-family: Calibri, Helvetica, arial, sans-serif;
                        }

                        h3 {
                            font-family: Cambria, "Times New Roman", serif;
                        }

                        text {
                            align: center;
                        }
                    </style>

                    <font color="white">
                        <center>KELURAHAN KARAMAT<br> KOTA SUKABUMI
                </h1> <br>
                <h2>Jl. Bhineka Karya No.37/116 (0266) 231679</h2>
                </CENTER>
                </font>

            </div>

            <div class="left-div">
                <div class="user-settings-wrapper">
                    <div class="navbar-header">

                        <a class="navbar-brand" href="index.html">


                        </a>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="notice-board">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <blink><span style="color: #0000FF;">
                                    <center>Aplikasi Pengaduan Masyarakat</center>
                                </span></blink>

                        </div>
                        <div class="panel-body">

                            <center>
                                <h1>
                                    <font color="black" </font> <marquee behavior="scroll" direction="LEFT"> Selamat
                                        Datang Di Website Kelurahan Karamat</marquee>
                                </h1>
                            </center>
                            <div class="container-fluid">
                                <div class="row">
                                    <div id="carousel" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <center><img class="d-block w-50"
                                                        src="<?php echo base_url('assets/beranda/') ?>foto/569938f7-81c9-410b-b190-32d651893ddf.JPG"
                                                        alt="First slide">
                                            </div>
                                            <div class="carousel-item">
                                                <center> <img class="d-block w-50"
                                                        src="<?php echo base_url('assets/beranda/') ?>foto/abed4628-d50d-4321-959e-e9b161792908.JPG"
                                                        alt="Second slide">
                                            </div>
                                            <div class="carousel-item">
                                                <center><img class="d-block w-50"
                                                        src="<?php echo base_url('assets/beranda/') ?>foto/bcf26a39-8893-4814-a9f2-269beedd621b.JPG"
                                                        alt="Third slide">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                            data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                            data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <!-- .pogoSlider -->
                                </div>
                                <br />
                                <hr /><br />
                                <div class="row" align="center">
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-aqua">
                                            <div class="inner">
                                                <h3><?php echo $proses ?></h3>

                                                <p>Laporan Sedang Di Proses</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-bag"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-green">
                                            <div class="inner">
                                                <h3><?php echo $selesai ?></h3>

                                                <p>Laporan Selesai</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-stats-bars"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-red">
                                            <div class="inner">
                                                <h3><?php echo $terabaikan ?></h3>

                                                <p>Laporan Terabaikan</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-person-add"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                    <div class="col-lg-3 col-xs-6">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3><?php echo $total ?></h3>

                                                <p>Total Laporan Masuk</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-person-add"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col -->
                                </div>
                            </div>


                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2020 Kel. Karamat | By : <a href="http://www.designbootstrap.com/" target="_blank">KELURAHAN
                        KARAMAT</a>
                </div>

            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

</body>

</html>