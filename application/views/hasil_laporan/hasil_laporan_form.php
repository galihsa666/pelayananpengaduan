<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Pengaduan Masyarakat</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <tr>
                        <td width="200">Nama Pelapor</td>
                        <td>: <?php echo $laporan_masyarakat['full_name'] ?></td>
                    </tr>
                    <tr>
                        <td>
                            Lokasi Kejadian
                        </td>
                        <td>
                            : <?php echo $laporan_masyarakat['lokasi_kejadian'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal - Jam Kejadian</td>
                        <td>: <?php echo date('Y-m-d', strtotime($laporan_masyarakat['tgl_kejadian'])) ?> -
                            <?php echo date('H:i:s', strtotime($laporan_masyarakat['jam_kejadian'])) ?></td>
                    </tr>
                    <tr>
                        <td>Nominal Kerugian</td>
                        <td>: Rp. <?php echo number_format($laporan_masyarakat['nominal_kerugian']) ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Laporan</td>
                        <td>: <?php echo date('Y-m-d H:i:s', strtotime($laporan_masyarakat['tanggal'])) ?></td>
                    </tr>
                    <tr>
                        <td>Status Laporan</td>
                        <td>: <?php echo $laporan_masyarakat['status_laporan'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Isi Laporan : <br />
                            <p><?php echo $laporan_masyarakat['isi_laporan'] ?></p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Hasil Laporan</h3>
            </div>
            <div class="box-body">
                <?php
                    foreach($hasil as $hasil_data){
                ?>
                <p>
                    <b><?php echo $hasil_data->status_laporan." - ".$hasil_data->created_at ?></b>
                    <br>
                    <?php echo $hasil_data->hasil_laporan ?>
                    <hr>
                </p>
                <?php
                    }
                ?>
            </div>
            <div class="box-footer">
                <hr>
                <a href="<?php echo site_url('Hasil_laporan/cetak/'.$nolaporan) ?>" class="btn btn-info"><i
                        class="fa fa-print"></i> Cetak Hasil Laporan</a>
            </div>
        </div>

        <?php
            if($laporan_masyarakat['status_laporan'] != 'SELESAI' && $this->session->userdata('id_user_level') == 1){
        ?>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT HASIL LAPORAN</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">

                <table class='table table-bordered'>
                    <tr>
                        <td width='200'>Hasil Laporan <?php echo form_error('hasil_laporan') ?></td>
                        <td> <textarea class="form-control" rows="3" name="hasil_laporan" id="hasil_laporan"
                                placeholder="Hasil Laporan"><?php echo $hasil_laporan; ?></textarea></td>
                    </tr>
                    <tr>
                        <td width='200'>Status Laporan <?php echo form_error('status_laporan') ?></td>
                        <td>
                            <select name="status_laporan">
                                <option>PROSES</option>
                                <option>SELESAI</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" class="form-control" name="nolaporan" id="nolaporan"
                                placeholder="Nolaporan" value="<?php echo $nolaporan; ?>" />
                            <input type="hidden" name="no_hp" value="<?php echo $laporan_masyarakat['no_hp'] ?>">
                            <input type="hidden" name="notlhp" value="<?php echo $notlhp; ?>" />
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?php echo $button ?></button>
                    </tr>
                </table>
            </form>
        </div>
        <?php
            }
        ?>
    </section>
</div>