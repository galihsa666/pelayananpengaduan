<div class="content-wrapper">

    <section class="content">
        <center><img src="<?php echo base_url('assets/img/header.png') ?>"></center>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><center>Pengaduan Masyarakat</center></h3>
                <hr/>
            </div>
            <div class="box-body">
                <table class="table">
                    <tr>
                        <td width="200">Nama Pelapor</td>
                        <td>: <?php echo $laporan_masyarakat['full_name'] ?></td>
                    </tr>
                    <tr>
                        <td>
                            Lokasi Kejadian
                        </td>
                        <td>
                            : <?php echo $laporan_masyarakat['lokasi_kejadian'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal - Jam Kejadian</td>
                        <td>: <?php echo date('Y-m-d', strtotime($laporan_masyarakat['tgl_kejadian'])) ?> -
                            <?php echo date('H:i:s', strtotime($laporan_masyarakat['jam_kejadian'])) ?></td>
                    </tr>
                    <tr>
                        <td>Nominal Kerugian</td>
                        <td>: Rp. <?php echo number_format($laporan_masyarakat['nominal_kerugian']) ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Laporan</td>
                        <td>: <?php echo date('Y-m-d H:i:s', strtotime($laporan_masyarakat['tanggal'])) ?></td>
                    </tr>
                    <tr>
                        <td>Status Laporan</td>
                        <td>: <?php echo $laporan_masyarakat['status_laporan'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Isi Laporan : <br />
                            <p><?php echo $laporan_masyarakat['isi_laporan'] ?></p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><center>Hasil Laporan</center></h3>
                <hr/>
            </div>
            <div class="box-body">
                <?php
                    foreach($hasil as $hasil_data){
                ?>
                <p>
                    <b><?php echo $hasil_data->status_laporan." - ".$hasil_data->created_at ?></b>
                    <br>
                    <?php echo $hasil_data->hasil_laporan ?>
                    <hr>
                </p>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>
</div>
<script>
    window.print();
</script>